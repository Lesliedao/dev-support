import logging, os
from datetime import datetime

def init_logger(name, level = 'DEBUG', output_dir = 'logs'):
    """
    Initializes a console and file logger.
    :param name: Name of the running process, to be prepended on each log line
    :param level: Level of the logging, default DEBUG
    :param output_dir: Directory where to put the log files, default logs
    :return: Logger object which can be passed around to keep logging to the same streams
    """

    logger = logging.getLogger(name)
    logger.setLevel(level)

    # Use a standard format when logging
    formatter = logging.Formatter('%(name)s | %(asctime)s | %(levelname)s | %(message)s')

    console_handler = logging.StreamHandler()
    console_handler.setLevel(level)
    console_handler.setFormatter(formatter)
    logger.addHandler(console_handler)

    # Use the datetime at the time of running the application as the filename for the log file
    date_time = datetime.now()
    log_file = str(date_time.year) + str(date_time.month).zfill(2) + str(date_time.day).zfill(2) + '-' + \
               str(date_time.hour).zfill(2) + str(date_time.minute).zfill(2) + '.log'
    file_handler = logging.FileHandler(os.path.join(output_dir, log_file))
    file_handler.setLevel(level)
    file_handler.setFormatter(formatter)
    logger.addHandler(file_handler)

    return logger